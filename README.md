# PHP助手函数

```shell
composer require lvzmen/helper
```

## 1. iArrayHelper
数组帮助函数，有如下内容：
- sum
- map
- sort
- toArray
- getValue
- setValue
- removeByKey
- removeByValue
- index
- getColumn
- map2
- keyExists
- multisort
- isAssociative
- isIndexed
- isIn
- isSubset
- filterByKey
- filterByValue
- setKeys
- setAllValuesToString
- trim

## 2. iFormatHelper
格式化助手，有如下内容：
- float
- remotePercent
- unSensible
- unCamelize
- camelize
- route
- routeCommon